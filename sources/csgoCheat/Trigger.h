#pragma once
#include "Memory.h"

class Trigger
{
private:
	// recovery times for every weapon
	int cz75 = 243;
	int desertEagle = 812;
	int dualBerettas = 525;
	int fiveSeven = 201;
	int glock18 = 201;
	int p2000 = 350;
	int p250 = 346;
	int tec9 = 392;
	int usps = 350;
	int ak47 = 369;
	int aug = 430;
	int awp = 350;
	int famas = 251;
	int g3sg1 = 545;
	int galilAr = 301;
	int m4a4 = 339;
	int m4a1s = 339;
	int scar20 = 545;
	int sg553 = 453; 
	int ssg08 = 143;
	int mac10 = 400;
	int mp5sd = 438;
	int mp7 = 438;
	int mp9 = 258;
	int ppBizon = 332;
	int p90 = 373;
	int ump45 = 350;
	int mag7 = 400;
	int nova = 461;
	int sawedOff = 461;
	int xm1014 = 506;
	int m249 = 829;
	int negev = 301;

	int GetRecoveryTime(DWORD _localPlayer);
public:
	void Run();
};

extern Trigger trigger;